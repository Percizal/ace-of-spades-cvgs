﻿using AceOfSpadesCVGS.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AceOfSpadesCVGS.Tests.Controllers
{
    [TestClass]
    public class CVGS_gameMemberControllerTest
    {
        [TestMethod]
        public void CreateCVGSgameMember_View()
        {
            // Arrange
            CVGS_gameMemberController controller = new CVGS_gameMemberController();
            // Act
            ViewResult result = controller.Create() as ViewResult;
            // Assert
            Assert.AreEqual("", result.ViewName);
        }
        [TestMethod]
        public void DetailsCVGSgameMember_View()
        {
            // Arrange
            CVGS_gameMemberController controller = new CVGS_gameMemberController();
            // Act
            HttpStatusCodeResult result = (HttpStatusCodeResult)controller.Details(null);
            // Assert
            Assert.AreEqual(400, result.StatusCode);
        }
        
        [TestMethod]
        public void DeleteCVGSgameMember_View()
        {
            // Arrange
            CVGS_gameMemberController controller = new CVGS_gameMemberController();
            // Act
            HttpStatusCodeResult result = (HttpStatusCodeResult)controller.Delete(null);
            // Assert
            Assert.AreEqual(400, result.StatusCode);
        }

    }

}
