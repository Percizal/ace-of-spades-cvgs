﻿using AceOfSpadesCVGS.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AceOfSpadesCVGS.Tests.Controllers
{
    [TestClass]
    public class CartControllerTest
    {
        [TestMethod]
        public void IndexCart_View()
        {
            // Arrange
            CartController controller = new CartController();
            // Act
            ViewResult result = controller.Index() as ViewResult;
            // Assert
            Assert.AreEqual("", result.ViewName);
        }
        [TestMethod]
        public void CartList_NullId()
        {
            // Arrange
            CartController controller = new CartController();
            // Act
            HttpStatusCodeResult result = (HttpStatusCodeResult)controller.CartList(null);
            // Assert
            Assert.AreEqual(400, result.StatusCode);
        }
        [TestMethod]
        public void DeleteCartItem_NullId()
        {
            // Arrange
            CartController controller = new CartController();
            // Act
            HttpStatusCodeResult result = (HttpStatusCodeResult)controller.DeleteCartItem(null);
            // Assert
            Assert.AreEqual(400, result.StatusCode);
        }
        [TestMethod]
        public void checkout_View()
        {
            // Arrange
            CartController controller = new CartController();
            // Act
            ViewResult result = controller.checkout() as ViewResult;
            // Assert
            Assert.AreEqual("", result.ViewName);
        }
        [TestMethod]
        public void shipping_View()
        {
            // Arrange
            CartController controller = new CartController();

            // Act
            ViewResult result = controller.shipping() as ViewResult;
            // Assert
            Assert.AreEqual("", result.ViewName);
        }
        [TestMethod]
        public void confirmation_View()
        {
            // Arrange
            CartController controller = new CartController();
            // Act
            ViewResult result = controller.confirmation() as ViewResult;
            // Assert
            Assert.AreEqual("", result.ViewName);
        }
    }
}
