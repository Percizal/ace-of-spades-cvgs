﻿using AceOfSpadesCVGS.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AceOfSpadesCVGS.Tests.Controllers
{
    [TestClass]
    class ReviewControllerTest
    {
        [TestMethod]
        public void AddReview_View()
        {
            // Arrange
            ReviewController controller = new ReviewController();
            // Act
            ViewResult result = controller.Add(1) as ViewResult;
            // Assert
            Assert.AreEqual("Add", result.ViewName);
        }
        [TestMethod]
        public void PendingReview_View()
        {
            // Arrange
            ReviewController controller = new ReviewController();
            // Act
            ViewResult result = controller.Pending() as ViewResult;
            // Assert
            Assert.AreEqual("", result.ViewName);
        }
        [TestMethod]
        public void EditReview_NullId()
        {
            // Arrange
            ReviewController controller = new ReviewController();
            int? myInt = null;
            // Act
            HttpStatusCodeResult result = (HttpStatusCodeResult)controller.Edit(myInt);
            // Assert
            Assert.AreEqual(400, result.StatusCode);
        }
        [TestMethod]
        public void DeleteReview_NullId()
        {
            // Arrange
            ReviewController controller = new ReviewController();
            int? myInt = null;
            // Act
            HttpStatusCodeResult result = (HttpStatusCodeResult)controller.Delete(myInt);
            // Assert
            Assert.AreEqual(400, result.StatusCode);
        }

    }
}
