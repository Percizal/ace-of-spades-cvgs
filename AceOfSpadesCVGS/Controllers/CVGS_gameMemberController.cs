﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AceOfSpadesCVGS.Models;

namespace AceOfSpadesCVGS.Controllers
{
    public class CVGS_gameMemberController : Controller
    {
        private cvgsEntities1 db = new cvgsEntities1();

        // GET: CVGS_gameMember
        public ActionResult Index()
        {
            return View(db.CVGS_game.ToList());
        }

        public ActionResult Download()
        {
            return View("Download");
        }

        // GET: CVGS_gameMember/Details/5
        public ActionResult Details(int? id)
        {   
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CVGS_game cVGS_game = db.CVGS_game.Find(id);
            if (cVGS_game == null)
            {
                return HttpNotFound();
            }
            ViewBag.game = cVGS_game.game_id;
            return View(cVGS_game);
        }

        // GET: CVGS_gameMember/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CVGS_gameMember/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "game_id,game_name,ESRB_rating,game_description,game_release,user_rating,game_catergory,game_developer")] CVGS_game cVGS_game)
        {
            if (ModelState.IsValid)
            {
                db.CVGS_game.Add(cVGS_game);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cVGS_game);
        }

        // GET: CVGS_gameMember/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CVGS_game cVGS_game = db.CVGS_game.Find(id);
            if (cVGS_game == null)
            {
                return HttpNotFound();
            }
            return View(cVGS_game);
        }

        // POST: CVGS_gameMember/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "game_id,game_name,ESRB_rating,game_description,game_release,user_rating,game_catergory,game_developer")] CVGS_game cVGS_game)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cVGS_game).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cVGS_game);
        }

        // GET: CVGS_gameMember/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CVGS_game cVGS_game = db.CVGS_game.Find(id);
            if (cVGS_game == null)
            {
                return HttpNotFound();
            }
            return View(cVGS_game);
        }

        // POST: CVGS_gameMember/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CVGS_game cVGS_game = db.CVGS_game.Find(id);
            db.CVGS_game.Remove(cVGS_game);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
