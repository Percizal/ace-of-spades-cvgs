﻿using AceOfSpadesCVGS.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AceOfSpadesCVGS.Controllers
{
    public class CartController : Controller
    {
        private cvgsEntities1 db = new cvgsEntities1();
        //private ApplicationUserManager _userManager;

        //public CartController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        //{
        //    UserManager = userManager;
        //    //RoleManager = roleManager;

        //}

        //public ApplicationRoleManager RoleManager
        //{
        //    get
        //    {
        //        return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
        //    }
        //    private set
        //    {
        //        _roleManager = value;
        //    }
        //}

       
        //public ApplicationUserManager UserManager
        //{
        //    get
        //    {
        //        return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
        //    }
        //    private set
        //    {
        //        _userManager = value;
        //    }
        //}




        // GET: Cart
        public ActionResult Index()
        {
            
            
            return View();
        }

        public ActionResult CartList(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (Session["Cart"] == null)
            {
                List<Cart> CartList = new List<Cart>
                {
                    new Cart(db.CVGS_game.Find(id),1)
            };
                Session["Cart"] = CartList;
            }
            else
            {
                List<Cart> CartList = (List<Cart>)Session["Cart"];
                int check = cartCheck(id);
                if (check == -1)
                {
                    CartList.Add(new Cart(db.CVGS_game.Find(id), 1));
                }
                else
                {
                    CartList[check].Quantity++;
                    Session["Cart"] = CartList;
                }

            }
            return View("Index");
        }
        private int cartCheck(int? id)
        {
            List<Cart> CartList = (List<Cart>)Session["Cart"];
            for (int i = 0; i < CartList.Count; i++)
            {
                if (CartList[i].CVGS_Game.game_id == id)
                    return i;
            }
            return -1;
        }


        public ActionResult DeleteCartItem(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int check = cartCheck(id);
            List<Cart> CartList = (List<Cart>)Session["Cart"];
            CartList.RemoveAt(check);
            return View("Index");
        }


        public ActionResult checkout()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult checkout([Bind(Include = "transaction_id,payment_method,transaction_firstName,transaction_lastName,transaction_city,transaction_pc,payment_cc")] CVGS_Payment_Transaction cVGS_Payment_Transaction, bool? shipping_method)

        {
            
            string errorList;
            string cc = cVGS_Payment_Transaction.payment_cc.ToString();
            
            if (ModelState.IsValid)
            {
                try
                {
                  
                    db.CVGS_Payment_Transaction.Add(cVGS_Payment_Transaction);
                    if (cVGS_Payment_Transaction.transaction_id < 0)
                    {
                        errorList = "Id Must Be Greater Then 0!" + "\n";
                        RedirectToAction("checkout");
                    }
                    if (cc.Length > 16 || cc.Length < 15)
                    {
                        ViewBag.Error = "Not A valid credit card number!" + "\n";
                        RedirectToAction("checkout");
                    }
                    if (Regex.IsMatch(cVGS_Payment_Transaction.transaction_pc, "\\A[ABCEGHJKLMNPRSTVXY]\\d[A-Z] ?\\d[A-Z]\\d\\z") == false)
                    {
                        ViewBag.Error = "Not A valid postal code!" + "\n";
                        RedirectToAction("checkout");
                    }
                    db.SaveChanges();
                    Session["T"] = cVGS_Payment_Transaction.transaction_id;
                    if (shipping_method == false)
                    {
                        Session["S"] = "1";
                        Session["Physical"] = "Digital";
                        return View("confirmation");

                    }
                   else if (shipping_method == true)
                    {
                        Session["Physical"] = "Not Processed";
                        return View("shipping");
                    }

                }
                catch
                {
                    ViewBag.Error = "Id in use!";
                    RedirectToAction("Index");
                }
            }

            return View("Index");
        }

        // POST: Profile/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        public ActionResult shipping()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult shipping([Bind(Include = "shipping_id,shipping_firstName,shipping_lastName,shipping_city,shipping_pc,shippping_street,shipping_house_number")] CVGS_Shipping cVGS_Shipping)
        {
            
            if (ModelState.IsValid)
            {
                try
                {
                   
                    db.CVGS_Shipping.Add(cVGS_Shipping);
                    if (cVGS_Shipping.shipping_id < 0)
                    {
                        ViewBag.Error += "Id Must Be Greater Then 0!" + "\n";
                        RedirectToAction("shipping");
                    }

                    if (Regex.IsMatch(cVGS_Shipping.shipping_pc, "\\A[ABCEGHJKLMNPRSTVXY]\\d[A-Z] ?\\d[A-Z]\\d\\z") == false)
                    {
                        ViewBag.Error += "Not A valid postal code!" + "\n";
                        RedirectToAction("shipping");
                    }
                    db.SaveChanges();
                   Session["S"] = cVGS_Shipping.shipping_id;
                    return View("confirmation");
                }
                catch
                {
                    ViewBag.Error = "Id in use!";
                    RedirectToAction("Index");
                }
            }
            return View("Index");
        }
        public ActionResult confirmation()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult confirmation([Bind(Include = "order_id,order_date,order_payment,order_email,order_billing,order_shipping,order_user_id")] CVGS_order cVGS_order)
        {
            try
            {


                    cVGS_order.order_date = DateTime.Today;
                     cVGS_order.order_shipping = Convert.ToInt32(Session["S"]);
                   
                    cVGS_order.order_billing = Convert.ToInt32(Session["T"]);
                    string user_id = User.Identity.GetUserId();
                    cVGS_order.order_user_id = user_id;

                    db.CVGS_order.Add(cVGS_order);

                    if (cVGS_order.order_id < 0)
                    {
                        ViewBag.Error += "Id Must Be Greater Then 0!" + "\n";
                        RedirectToAction("shipping");
                    }
                
                    db.SaveChanges();
                Session["order_ID"] = cVGS_order.order_id;
                Session["user_id"] = user_id;
                RedirectToAction("order", "order");
            }
                catch
                {
                    ViewBag.Error = "Id in use!";
                    RedirectToAction("Home");
                }

            
            return RedirectToAction("order", "order");
        }

        //public async Task<ActionResult> EmailDownload()
        //{
        //    if (ModelState.IsValid)
        //    {
        //        string userId = User.Identity.GetUserId();
        //        await UserManager.SendEmailAsync(userId, "Download", "Download link here:");
        //        return RedirectToAction("Download", "CVGS_gameMember");
        //    }
        //    return View("Index");
        //}

        
    }
}