﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AceOfSpadesCVGS.Models;
using Microsoft.AspNet.Identity;

namespace AceOfSpadesCVGS.Controllers
{
    public class FriendsController : Controller
    {
        private cvgsEntities1 db = new cvgsEntities1();
        public ActionResult ListOfFriends()
        {
            string userId = User.Identity.GetUserId();
            var email = db.AspNetUsers.Where(u => u.Id == userId).Select(a => a.Email).Single();

            return View(db.CVGS_FriendsOrFamily.Where(u => u.senderEmail == email && u.isAccepted).ToList());
        }
        public ActionResult ListOfFamily()
        {
            string userId = User.Identity.GetUserId();
            var email = db.AspNetUsers.Where(u => u.Id == userId).Select(a => a.Email).Single();

            return View(db.CVGS_FriendsOrFamily.Where(u => u.senderEmail == email && u.isAccepted && u.isFamily).ToList());
        }

        // GET: Friends
        public ActionResult PendingOut()
        {
            string userId = User.Identity.GetUserId();
            var email = db.AspNetUsers.Where(u => u.Id == userId).Select(a => a.Email).Single();
            
            return View(db.CVGS_FriendsOrFamily.Where(u => u.senderEmail == email && !u.isAccepted).ToList());
        }
        public ActionResult PendingIn()
        {
            string userId = User.Identity.GetUserId();
            var email = db.AspNetUsers.Where(u => u.Id == userId).Select(a => a.Email).Single();
            return View(db.CVGS_FriendsOrFamily.Where(u => u.recieverEmail == email && !u.isAccepted).ToList());
        }
        // GET: Friends/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CVGS_FriendsOrFamily cVGS_FriendsOrFamily = db.CVGS_FriendsOrFamily.Find(id);
            if (cVGS_FriendsOrFamily == null)
            {
                return HttpNotFound();
            }
            return View(cVGS_FriendsOrFamily);
        }

        // GET: Friends/Create
        public ActionResult Add()
        {
            return View();
        }

        // POST: Friends/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add([Bind(Include = "requestId,senderEmail,recieverEmail,isAccepted,isBlocked,isFamily")] CVGS_FriendsOrFamily cVGS_FriendsOrFamily)
        {
            cVGS_FriendsOrFamily.isBlocked = false;
            cVGS_FriendsOrFamily.isAccepted = false;
            string userId = User.Identity.GetUserId();
            var emailSender = db.AspNetUsers.Where(u => u.Id == userId).Select(a => a.Email).Single();
            var emailReciver = db.AspNetUsers.Where(u => u.Email == cVGS_FriendsOrFamily.recieverEmail).Single().ToString();
            var exists = db.CVGS_FriendsOrFamily.Where(u => u.recieverEmail == emailReciver && u.isAccepted);
            if(exists == null)
            {
                cVGS_FriendsOrFamily.senderEmail = emailSender;
                db.CVGS_FriendsOrFamily.Add(cVGS_FriendsOrFamily);
                db.SaveChanges();
            }
            TempData["Exists"] = "This Person is already a friend!";
            return View("Add");

        }

        // GET: Friends/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CVGS_FriendsOrFamily cVGS_FriendsOrFamily = db.CVGS_FriendsOrFamily.Find(id);
            if (cVGS_FriendsOrFamily == null)
            {
                return HttpNotFound();
            }
            return View(cVGS_FriendsOrFamily);
        }

        // POST: Friends/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "requestId,senderEmail,recieverEmail,isAccepted,isBlocked,isFamily")] CVGS_FriendsOrFamily cVGS_FriendsOrFamily)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cVGS_FriendsOrFamily).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Manage");
            }
            return View(cVGS_FriendsOrFamily);
        }

        // GET: Friends/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CVGS_FriendsOrFamily cVGS_FriendsOrFamily = db.CVGS_FriendsOrFamily.Find(id);
            if (cVGS_FriendsOrFamily == null)
            {
                return HttpNotFound();
            }
            return View(cVGS_FriendsOrFamily);
        }

        // POST: Friends/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CVGS_FriendsOrFamily cVGS_FriendsOrFamily = db.CVGS_FriendsOrFamily.Find(id);
            db.CVGS_FriendsOrFamily.Remove(cVGS_FriendsOrFamily);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
