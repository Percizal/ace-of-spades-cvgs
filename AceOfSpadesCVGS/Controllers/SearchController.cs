﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AceOfSpadesCVGS.Models;

namespace AceOfSpadesCVGS.Controllers
{
    public class SearchController : Controller
    {
        private cvgsEntities1 db = new cvgsEntities1();

        // GET: Search
        public ActionResult Index(string search_txt)
        {
            return View(db.CVGS_game.Where(x=>x.game_name == search_txt || search_txt == null).ToList());
        }
    }
}
