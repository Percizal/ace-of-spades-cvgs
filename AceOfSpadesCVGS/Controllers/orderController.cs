﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AceOfSpadesCVGS.Models;
using Microsoft.AspNet.Identity;

namespace AceOfSpadesCVGS.Controllers
{
    public class orderController : Controller
    {

        private cvgsEntities1 db = new cvgsEntities1();
        public ActionResult Index()
        {
            return View(db.CVGS_Order_Detail.ToList());
        }
        // GET: order
        public ActionResult order([Bind(Include = "order_id,order_user_id,game_id,order_total,order_quantity,order_process")] CVGS_Order_Detail cVGS_Order_Detail)
        {
            try
            {


                List<Cart> CartList = (List<Cart>)Session["Cart"];
                for (int i = 0; i < CartList.Count; i++)
                {
                    cVGS_Order_Detail.order_id = Convert.ToInt32(Session["order_ID"]);
                    cVGS_Order_Detail.order_user_id = Session["user_id"] as string;
                    cVGS_Order_Detail.game_id = Convert.ToInt32(CartList[i].CVGS_Game.game_id);
                    cVGS_Order_Detail.order_quantity = CartList[i].Quantity;
                    cVGS_Order_Detail.order_process = Session["Physical"] as string;
                    cVGS_Order_Detail.order_total = Convert.ToDecimal(CartList[i].CVGS_Game.game_price*CartList[i].Quantity);
                    //cVGS_Order_Detail.order_total = cVGS_Order_Detail.order_quantity * CartList[i].CVGS_Game;

                    db.CVGS_Order_Detail.Add(cVGS_Order_Detail);
                    Session.Remove("Cart");
                    Session.Remove("S");
                    Session.Remove("T");
                }
                db.SaveChanges();
            }
            catch
            {
                ViewBag.Error = "Id in use!";
                RedirectToAction("Home");
            }
            string user = User.Identity.GetUserId();
            return View(db.CVGS_Order_Detail.Where(x => x.order_user_id == user).ToList());
        }
        public ActionResult orderCustomer()
        {

            
          string user = User.Identity.GetUserId();
          return View(db.CVGS_Order_Detail.Where(x => x.order_user_id == user).ToList());
        }
        public ActionResult Order_Admin()
        {
            return View(db.CVGS_Order_Detail.ToList());
        }
        // GET: Order/Edit/5
        public ActionResult Edit(int? id, int? gameid)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CVGS_Order_Detail cVGS_Order = db.CVGS_Order_Detail.Find(id, gameid);
            if (cVGS_Order == null)
            {
                return HttpNotFound();
            }
            return View(cVGS_Order);
        }

        // POST: Review/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "order_id,order_user_id,game_id,order_total,order_quantity,order_process")] CVGS_Order_Detail cVGS_Order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cVGS_Order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Order_Admin");
            }
            return View(cVGS_Order);
        }
    }
}